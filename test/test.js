var _ = require('lodash');
var chai = require('chai');
var chaiAsPromised = require('chai-as-promised');
var fs = require('fs');
var path = require('path');
var winston = require('winston');

chai.use(chaiAsPromised);
chai.should();

describe('Soy CLI', function() {
    this.timeout(10000);
    winston.level = 'error';
    var cli = require('../app/cli');

    var options = {
        version: '4.1.2',
        basedir: __dirname,
        outdir: path.resolve(__dirname, '../soy')
    };

    describe('validates required options', function() {
        it('basedir', function() {
            return cli(_.omit(options, 'basedir')).compile().should.be.rejectedWith('basedir is required')
        });

        it('outdir', function() {
            return cli(_.omit(options, 'outdir')).compile().should.be.rejectedWith('outdir is required')
        });
    });

    it('downloads JAR from Maven repo', function() {
        this.timeout(60000);
        return cli(options).download().should.eventually.contain('atlassian-soy-cli-support-' + options.version + '.jar');
    });

    it('compiles example template', function() {
        return cli(options).compile().then(function() {
            fs.statSync(path.resolve(options.outdir, 'test.js')).isFile().should.equal(true)
        }).should.be.fulfilled;
    });

    it('is also possible to pass a render type', function() {
        return cli(_.extend({}, options, {
            type: 'render',
            extraArgs: {
                rootnamespace: 'testnamespace',
                data: 'andThis:And this.'
            }
        })).compile().then(function(execString) {
            fs.readFileSync(path.resolve(options.outdir, 'test.html'), 'utf-8').should.equal('Some reason this is here. And this.');
            execString.should.contain('--type "render"');
            execString.should.not.contain('--type "js"');
        }).should.be.fulfilled;
    });

    describe('extraArgs', function() {
        it('get passed through', function() {
            return cli(_.extend(options, {
                extraArgs: {
                    'extension': 'soy.js',
                    'use-ajs-context-path': true
                }
            })).compile().then(function(execString) {
                execString.should.contain('--use-ajs-context-path');
                execString.should.contain('--extension "soy.js"');
            }).should.be.fulfilled;
        });

        it('do not override main options', function() {
            var ignored = {
                'basedir': 'ignored_base',
                'o': 'ignored_outdir'
            };

            return cli(_.extend(options, { extraArgs: ignored })).compile().then(function(execString) {
                execString.should.contain('--basedir "'+ options.basedir +'"');
                execString.should.not.contain(ignored.basedir);
                execString.should.contain('--outdir "'+ options.outdir +'"');
                execString.should.not.contain(ignored.o);
            }).should.be.fulfilled;
        });

        it('support array values', function() {
            return cli(_.extend(options, {
                extraArgs: {
                    'i18n': [ 'a.properties', 'b.properties' ]
                }
            })).compile().then(function(execString) {
                execString.should.contain('--i18n "a.properties"');
                execString.should.contain('--i18n "b.properties"');
            }).should.be.fulfilled;
        });
    });
});
