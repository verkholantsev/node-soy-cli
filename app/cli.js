var _ = require('lodash');
var exec = require('child-process-promise').exec;
var findJavaHome = require('find-java-home');
var fs = require('fs-extra');
var https = require('https');
var interpolate = require('interpolate');
var path = require('path');
var Q = require('q');
var winston = require('winston');

/** {string} template URL whence the Soy CLI is downloaded (instances of {version} will be interpolated)*/
var OLD_MAVEN_URL_TEMPLATE = 'https://maven.atlassian.com/service/local/repositories/atlassian-public/content/com/atlassian/soy/atlassian-soy-cli-support/{version}/atlassian-soy-cli-support-{version}-jar-with-dependencies.jar';
var ARTIFACTORY_URL_TEMPLATE = 'https://packages.atlassian.com/maven/repository/public/com/atlassian/soy/atlassian-soy-cli-support/{version}/atlassian-soy-cli-support-{version}-jar-with-dependencies.jar';

var DEFAULTS = {
    /** {string} version of the Soy CLI to download */
    version: '4.1.2',

    /** {object} extra arguments to pass through (values can be string or string[]) */
    extraArgs: {},

    /** {String} default type is js */
    type: 'js'
};

module.exports = function(options) {
    options = _.defaults({}, options, DEFAULTS);

    return {
        options: _.extend({}, options),

        /**
         * Compiles all soy templates that match the given glob.
         *
         * @param {string} glob
         * @param {Function} [callback] done callback is using callback style
         * @return {Promise|undefined} a Promise if no callback is passed in, or undefined
         */
        compile: function(glob, callback) {
            return compile(glob).nodeify(callback)
        },

        /**
         * Downloads the configured version of Soy CLI.
         *
         * @param {Function} [callback] done callback is using callback style
         * @return {Promise|undefined} a Promise if no callback is passed in, or undefined
         */
        download: function(callback) {
            return download().nodeify(callback)
        }
    };

    function validateOptions() {
        if (!options.basedir) return Q.reject('basedir is required');
        if (!options.outdir) return Q.reject('outdir is required');

        return Q();
    }

    function compile(glob) {
        glob = glob || '*.soy';

        return validateOptions().then(function() {
            return download(options.version);
        }).then(function(soyCliJar) {
            return getJavaHome().then(function(javaHome) {
                winston.info('Java home is %s', javaHome);

                /** path to the java binary */
                var java = interpolate('"{java_home}"/bin/java -jar "{jar_path}"', {
                    java_home: javaHome,
                    jar_path: soyCliJar
                });

                var cmd = [java, makeArgString(compileArgs())].join(' ');
                winston.debug('About to exec: %s', cmd);

                return exec(cmd).then(function(result) {
                    winston.info('Soy CLI output follows:');
                    result.stdout && console.log(result.stdout);
                    result.stderr && console.log(result.stderr);

                    // returns the actual command that was run for testing purposes
                    return Q(cmd);
                });
            })
        });

        /**
         * @return {object} the effective args, derived from the options and any provided {@code extraArgs}
         */
        function compileArgs() {
            // these need to be passed in as options, not extra args
            var sanitisedExtraArgs = _.omit(options.extraArgs,
                't', 'type',
                'b', 'basedir',
                'o', 'outdir',
                'g', 'glob'
            );

            return _.extend({}, sanitisedExtraArgs, {
                type: options.type,
                basedir: options.basedir,
                outdir: options.outdir,
                glob: glob
            });
        }
    }

    function download() {
        var interpolateParams = {version: options.version};
        var localPath = path.resolve(__dirname, '../cache', interpolate('atlassian-soy-cli-support-{version}.jar', interpolateParams));

        function streamFromMavenRepo() {
            var url = interpolate(ARTIFACTORY_URL_TEMPLATE, interpolateParams);

            // adapt the callback-based API to a Promise
            var deferred = Q.defer();
            var file = fs.createOutputStream(localPath);
            winston.info("Downloading %s...", url);
            https.get(url, function(response) {
                response.pipe(file);
                file.on('finish', function() {
                    file.close(function() {
                        deferred.resolve(localPath);
                    });
                });
            }).on('error', function(err) {
                fs.unlink(dest);
                deferred.reject(err.message);
            });

            return deferred.promise;
        }

        return Q.ninvoke(fs, 'stat', localPath)
            .then(function() {
                // file exists
                winston.info('Found Soy CLI %s in %s', options.version, localPath);
                return localPath;
            }, function(err) {
                if (err.code == 'ENOENT') {
                    return streamFromMavenRepo();
                } else {
                    return err;
                }
            });
    }
};

/**
 * Converts an array of args into an arg string that can be passed to the Soy CLI as command line arguments.
 *
 * @param {object} args all arguments to pass
 * @return {string} a string suitable to pass to the Soy CLI
 */
function makeArgString(args) {
    function singleArgString(tuple) {
        if (tuple.length === 1) {
            return interpolate('--{flag}', {flag: tuple[0]});
        } else {
            return interpolate('--{key} "{value}"', {key: tuple[0], value: tuple[1]});
        }
    }

    /** transforms a key-value pair into a an array of "tuples" (1-tuple for flags, 2-tuple for options) */
    function toTuples(value, key) {
        if (_.isBoolean(value)) {
            return [[key]]
        } else if (_.isString(value)) {
            return [[key, value]];
        } else if (_.isArray(value)) {
            return value.map(function(element) {
                return [key, element];
            });
        } else {
            throw new Error('extraArgs must be one of: string, strings[], boolean');
        }
    }

    // flatten into a single string
    return _(args).map(toTuples).flatten().map(singleArgString).join(' ');
}

function getJavaHome() {
    return Q.nfcall(findJavaHome).then(function(javaHome) {
        // find-java-home is inserting a newline at eol, make sure it's not there.
        return javaHome ? javaHome.trim() : javaHome;
    });
}
